
    <?php $user_type = $this->session->userdata('USER_TYPE');
        if($user_type==LCS_Administrator||$user_type==Site_Admin||$user_type==IT_Administrator){

            $style='col-lg-3';


        }
        else{

              $style='col-lg-4';

        }
     ?>
        <div class="wrapper wrapper-content">
        <div class="row">
      
       <div class="<?php echo $style; ?>">
                <div class="widget style1 color_26247B">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-group fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <span>Total Online Users </span>
                            <h2 class="font-bold"><?php echo $online; ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if($user_type==LCS_Administrator||$user_type==Site_Admin||$user_type==IT_Administrator){
                ?>
            <div class="<?php echo $style; ?>">
                <div class="widget style1 color_674172">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <span>Total WiFi Users</span>
                            <h2 class="font-bold"><?php echo $adminuser; ?></h2>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
            <div class="<?php echo $style; ?>">
                <div class="widget style1 color_9F48AE">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-wifi fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <span>Today's WiFi Usage </span>
                            <h2 class="font-bold"><?php echo $totalusage; ?>GB</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="<?php echo $style; ?>">
                <div class="widget style1 color_947CB0">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-signal fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <span>Monthly WiFi Usage </span>
                            <h2 class="font-bold"><?php echo $monthusage; ?>GB</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
              
               <div class="row">

        <div class="col-lg-12">
        <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Failed Operations</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
             
             
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">

                       <div class="row">
        

                                 <div style="display: none;" class="form-group col-md-4" style="margin-bottom: 0px;"><label style="padding-left: 0px;padding-right: 0px;margin-top: 5px; margin-bottom: 0px;" class="col-lg-4 control-label">Select Hotel</label>

                  <div class="col-lg-8" style="padding-left: 0px; padding-right: 0px;">
                   <select name="hotel_names" id="hotel_names"   class="form-control input-sm">
                        <option value="<?php echo $this->session->userdata('REALM')?>" >--Please Select--</option>
                                      <?php foreach($hotels as $hotel){?>
                                        
                                        <option value="<?php echo $hotel->realm?>"><?php echo $hotel->hotelname?></option>
                                        
                                        <?php }?>
                   </select>
                 </div>
                                </div> 
          
            <div class="table-responsive">
                <table id="manage_user_table" class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                    <tr>

                        <th>Username</th>
                          <th>MAC</th>
                         <th> Reply</th>
                        <th> Message Details</th>
                        <th>Location</th>
                        <th>Attempt Time</th>
                        
                   
                    </tr>
                    </thead>
                    <tfoot align="right">
                        <tr><th></th><th></th><th></th><th></th><th></th><th></th></tr>
                    </tfoot>
                </table>

            </div>

        </div>
        </div>
        </div>

        </div>


                </div>
                
      
            <script src="<?php echo base_url(); ?>resources/js/jquery-2.1.1.js"></script> 
<script>
$(document).ready(function(){
   
    $.toast({
    heading: 'Welcome to Ramada Colombo',
    text: 'Wi-Fi System Admin Portal',
    showHideTransition: 'plain',
    hideAfter: 3000,
    position: 'top-right',
    bgColor: '#7b524e',
    icon: 'success', 
    width :'3px'                                          
    });
    


    getAccessReject();


    });

function getAccessReject(){



        var today = new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString().split('.')[0].replace('T',' ');
         


    manage_wifi_users = $('#manage_user_table').DataTable({


            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // computing column Total of the complete result 


             var count = api
                .column( 0 )
                .data()
                .count();
                
   

                 
            // Update footer by showing the total with the reference of the column index 
        $( api.column( 0 ).footer() ).html('Total ('+count+')');
           
        },





        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [

              {extend: 'copy',title: 'Access Rejects  ',footer: true,
   },

        {extend: 'csv',title: 'Access Rejects ',footer: true,
   },
   {extend: 'excel',title: 'Access Rejects ',footer: true,
      message: '__MESSAGE__',
   customize: function ( doc ) {
       doc.content.forEach(function(content) {
         if (content.style == 'message') {
            content.text = 'This Report was  generated on ' +today+''
         }
       })
    }, 
},
{extend: 'pdf',title: 'Access Rejects ',footer: true,
   message: '__MESSAGE__',
   customize: function ( doc ) {
       doc.content.forEach(function(content) {
         if (content.style == 'message') {
            content.text = 'This Report was  generated on ' +today+''
         }
       })
    }, 
},

{extend: 'print',title: 'Access Rejects ',footer: true,
customize: function (win){
    $(win.document.body).addClass('white-bg');
    $(win.document.body).css('font-size', '10px');

    $(win.document.body).find('table')
    .addClass('compact')
    .css('font-size', 'inherit');
    $(win.document.body).find( 'table');        
}
}
],
"processing" : true,
"serverSide" : false,
"order" : [],

"ajax" : {
    url:"<?php echo base_url('Dashboard/getAccessReject')?>",
    data :{hotel_name_ralm:$("#hotel_names").val()},
    "dataSrc":"",
    type:"POST",
},

"columns": [
{"mData":"username"},
{"mData":"mac"},
{"mData":"reply"},
{"mData":"message"},
{"mData":"calledstationid"},
{"mData":"authdate"}


],
"destroy" : true 
});


    
}



</script>
   


