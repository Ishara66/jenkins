@extends('templates.layout')


         @section('title')
         Nawaloka | Login
        @endsection
        

    	@section('content')
        <div class="text-center loginscreen middle-box-x animated fadeInDown">
            <div class="logo">
                <!--                <h1 class="logo-name">IN+</h1>-->
                <img src="{{asset("resources/img/login/logo.png")}}"  style="height: 80px;">
            </div>
            <div class="login-box">
                <h3>Welcome to Wi-Fis Admin Portal</h3>
                <h1>Sign In </h1>
                
                
                <form class="m-t" id="signin_form" name="signin-form" role="form">
                	  {{csrf_field()}}
                	  
                    <div class="form-group">
                      <input type="text" class="form-control" name="username" id="username"  placeholder="Username"/>
                    </div>
                    <div class="form-group">
                    <input type="password" class="form-control" name="password" name="password" placeholder="Password"/>
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b" style="height: 40px; margin-top: 80px;">Login</button>

                    <a href="#"><small>Forgot password?</small></a>
                    
                    
                </form>
            </div>
            <div class="logo-lcs">
                <!--                <h1 class="logo-name">IN+</h1>-->
                <img src="{{asset("resources/img/login/lankacom-logo.png")}}" >
            </div>
        </div>
        @endsection('content')


     <script src="{{asset("resources/js/jquery-2.1.1.js")}}"></script>
    

                    <script type="text/javascript">

                    $(document).ready(function() {

                        $("#signin_form").validate({
                            ignore: "",
                            rules: {
                                username: {required: true},
                                password: {required: true},
                            },
                            submitHandler: function(form) {

                                var $form = $('#signin_form');
                                $.ajax({
                                    type: 'POST',
                                    url: '/login',
                                    data: $form.serialize(),
                                
                                    success: function(msg) {
                                       
                                        if (msg != '0') {
                                            $.toast({
                                                 heading: 'Success',
                                                 text: 'Login successfull',
                                                 showHideTransition: 'plain',
                                                  hideAfter: 1500,
                                                  position: 'top-right',
                                                  bgColor: '#d21241',
                                                  icon: 'success'
                                                   });
                                            // $('#login_success_msg').fadeIn();
                                            // $('#login_success_msg').fadeOut(4000);
                                           // setTimeout("location.href = site_url+'/backend/dashboard/';", 1000);
                                           setTimeout("location.href = asset+'Dashboard/select_dashboard'", 2000);
                                        } else {
                                            // $('#login_error_msg').fadeIn();
                                            // $('#login_error_msg').fadeOut(4000);
                                            $.toast({
                                                 heading: 'Error',
                                                 text: 'Incorrect Username or Password',
                                                 showHideTransition: 'plain',
                                                 position: 'top-right',
                                                  hideAfter: 2000,
                                                  icon: 'error'
                                                   });
                                            signin_form.reset();
                                        }

                                    },
                                    error: function(msg) {
                                        $('#login_error_msg').fadeIn();
                                        $('#login_error_msg').fadeOut(4000);
                                    }
                                });
                            }
                        });
                    });

                    </script>
              
</html>
