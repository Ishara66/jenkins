<?php header('Access-Control-Allow-Origin: *'); ?>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>Ramada Colombo Wifi Management Prtal</title>
        <!-- JS file -->


      <link rel="icon" href="{{asset("resources/img/logo.png")}}" type="image/x-icon"/>
      <link href="{{asset("resources/css/bootstrap.min.css")}}" rel="stylesheet">
    
    <link rel="stylesheet" href="{{asset("resources/js/easy-autocomplete.themes.min.css")}}"> 
     <link rel="stylesheet" href="{{asset("resources/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css")}}">
     <link href="{{asset("resources/css/plugins/iCheck/custom.css")}}" rel="stylesheet">
      <link href="{{asset("resources/css/plugins/select2/select2.min.css")}}" rel="stylesheet">
    
    <!-- <link href="<?php echo base_url(); ?>resources/css/plugins/sweetalert/sweetalert.css" rel="stylesheet"> -->
    <link href="{{asset("resources/font-awesome/css/font-awesome.css")}}" rel="stylesheet">
    <link href="{{asset("resources/css/animate.css")}}" rel="stylesheet">
    <link href="{{asset("resources/css/style.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset("resources/css/custom.css")}}">
    <link href="{{asset("resources/css/plugins/toastr/jquery.toast.min.css")}}" rel="stylesheet">
     <link href="{{asset("resources/css/plugins/dataTables/datatables.min.css")}}" rel="stylesheet">
     <link href="{{asset("resources/css/plugins/datapicker/datepicker3.css")}}" rel="stylesheet">
     <link href="{{asset("resources/css/plugins/bootstrap_tagsinput/bootstrap_tagsinput.css")}}" rel="stylesheet">
    

</head>

<body>
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                 <img src="{{asset("resources/img/logo.png")}}" alt="">

                </li>
                

 

                <li class="active">
                    <a href="<?php echo site_url() ?>Dashboard/select_dashboard">
                        <i class="fa fa-th-large"></i> 
                        <span class="nav-label" >Dashboard</span> </a>
      
                </li>
                <?php foreach($prev_catagory as $catogary){?>
                <li>
                    <a href=""><i class="<?php echo $catogary->fa_icon?>"></i> <span class="nav-label"><?php echo $catogary->category_name?></span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                                <?php foreach($priveladge as $row){?>
                                <?php if($catogary->category_id == $row->privilege_category_id){?>
                                <li ><a href="<?php echo base_url("$row->url") ?>"><?php echo $row->privilege_name?></a></li>
                                <?php }}?>
                            </ul>
                </li>
                <?php }?>
           
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn color_9F48AE " href="#"><i class="fa fa-bars"></i> </a>
           <!--  <form role="search" class="navbar-form-custom" action="https://www.google.com/search?">
                <div class="form-group">
                <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form> -->
        </div>
            <ul class="nav navbar-top-links navbar-right">
                
                  <li>
                    <!-- <span class="m-r-sm text-muted welcome-message">Welcome to Cinnamon Hotels Wi-Fi Portal</span> -->
                </li>
                  <li>
                    
                    <div class="dropdown profile-element">
                  <!--    <span>
                            <img alt="image" class="img-circle" src="<?php echo base_url(); ?>resources/img/profile_small.jpg" />
                             </span> -->
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                             <span style="color:#101010;" class="text-muted text-xs block"><?php echo $this->session->userdata('USERNAME');?> <b class="caret"></b></span></a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <!-- <li><a href="profile.html">Profile</a></li> -->
                            <li><a data-toggle="modal" data-target="#change_password_model">Change Password</a></li>
                            <!-- <li><a href="mailbox.html">Mailbox</a></li> -->
                            
                            <li class="divider"></li>
                            <li><a href="<?php echo site_url('Login/logout'); ?>">Logout</a></li>
                        </ul>
                    </div>
              

                </li>

<!-- 
                <li>
                    <a href="login.html">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li> -->

              
              
            </ul>

        </nav>
        </div>
      
     <div class="page-content">
            <?php echo $content; ?>

                                    
    </div>
       
        </div>

          <div class="footer fixed">
                <div class="pull-right">
                    <strong>Ramada Colombo</strong> 
                </div>
                <div>
                    <strong>Powered</strong> By LankaCom © <?php echo date('Y')?>
                </div>
            </div>
       
<!-- change Password Model -->
    <div class="modal inmodal fade" id="change_password_model" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                               
                               <div class="modal-dialog modal-md">
                                   <div class="modal-content">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                           <h4 class="modal-title">Change Password</h4>
                                           
                                       </div>
                                       <form role="form" id="change_password_form">
                                       <div class="modal-body">
                                       
                                           <div class="form-group"><label>Old Password</label> 
                                           <input name="old_password" id="old_password" placeholder="Old Password" class="form-control" type="password"></div>
                                         <div class="form-group"><label>New Password</label> 
                                         <input name="new_password" id="new_password" placeholder="New Password" class="form-control" type="password"></div>
                                         <div class="form-group"><label>Confirm Password</label>
                                          <input name="confm_password" id="confm_password" placeholder="Confirm Password" class="form-control" type="password"></div>
                                                                                                        
                                        </div>
                                      
                                       <div class="modal-footer">
                                           <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                           <button   id="change_password_btn"  type="button" style="background-color:#b46fc3;    border-color: #b46fc3;" class="btn btn-primary">Save changes</button>
                                       </div>
                                       </form>  
                                   </div>
                               </div>
                               
                           </div>
                           

   <!-- Mainly scripts -->
 <!--    <script src="<?php echo base_url(); ?>resources/js/jquery-2.1.1.js"></script> -->

    <script src="{{asset("resources/js/jquery-ui-1.10.4.min.js")}}"></script>
    <script src="{{asset("resources/js/bootstrap.min.js")}}"></script>
    <!-- <script src="<?php echo base_url(); ?>resources/js/plugins/sweetalert/sweetalert.min.js"></script> -->
    <script src="{{asset("resources/js/plugins/metisMenu/jquery.metisMenu.js")}}"></script>
    <script src="{{asset("resources/js/plugins/slimscroll/jquery.slimscroll.min.js")}}"></script>
    <script src="{{asset("resources/js/jquery.validate.min.js")}}"></script>
    <!-- Flot -->
    <script src="{{asset("resources/js/plugins/flot/jquery.flot.js")}}"></script>
    <script src="{{asset("resources/js/plugins/flot/jquery.flot.tooltip.min.js")}}"></script>
    <script src="{{asset("resources/js/plugins/flot/jquery.flot.spline.js")}}"></script>
    <script src="{{asset("resources/js/plugins/flot/jquery.flot.resize.js")}}"></script>
    <script src="{{asset("resources/js/plugins/flot/jquery.flot.pie.js")}}"></script>
    <script src="{{asset("resources/js/plugins/flot/jquery.flot.symbol.js")}}"></script>
    <script src="{{asset("resources/js/plugins/flot/jquery.flot.time.js")}}"></script>

    <!-- Peity -->
    <script src="{{asset("resources/js/plugins/peity/jquery.peity.min.js")}}"></script>
    <script src="{{asset("resources/js/demo/peity-demo.js")}}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{asset("resources/js/inspinia.js")}}"></script>
    <script src="{{asset("resourcess/js/plugins/pace/pace.min.js")}}"></script>

    <!-- jQuery UI -->
    <script src="{{asset("resources/js/plugins/jquery-ui/jquery-ui.min.js")}}"></script>

    <!-- Jvectormap -->
    <script src="{{asset("resources/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js")}}"></script>
    <script src="{{asset("resources/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")}}"></script>

    <!-- EayPIE -->
    <script src="{{asset("resources/js/plugins/easypiechart/jquery.easypiechart.js")}}"></script>

    <!-- Sparkline -->
    <script src="{{asset("resources/js/plugins/sparkline/jquery.sparkline.min.js")}}"></script>
    <script src="{{asset("resources/js/plugins/toastr/jquery.toast.js")}}"></script>
     
     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Sparkline demo data  -->
    <script src="{{asset("resources/js/demo/sparkline-demo.js")}}"></script>
    <script src="{{asset("resources/js/plugins/dataTables/datatables.min.js")}}"></script>
      <!-- iCheck -->
    <script src="{{asset("resources/js/plugins/iCheck/icheck.min.js")}}"></script>
    <script src="{{asset("resources/js/plugins/chosen/chosen.jquery.js")}}"></script>
    <script src="{{asset("resources/js/plugins/datapicker/bootstrap-datepicker.js")}}"></script>
    <script src="{{asset("resources/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js")}}"></script>

      



      
    <script>
 
 $('#change_password_btn').on('click', function() {
   
   if ($("#change_password_form").valid()){
       $.ajax({
        type : 'POST',
        url : "<?php echo base_url('index.php/Admin/password_change'); ?>" ,
        data : {new_password :$('#new_password').val()},

        success: function(res){
           
            $('#change_password_model').modal('hide');
            $("#change_password_form")[0].reset();
            if(res == "true"){
                $.toast({
                heading: 'Success',
                text: 'You have Successfully Change Your Password',
                showHideTransition: 'plain',
                hideAfter: 2500,
                position: 'top-right',
                bgColor: '#9f48ae',
                icon: 'success'
             });  
            }
          
        }
    });
   }

});

// $('#add_admin_btn').on('click', function() {
//    alert('hdsjgh');
// });

// change password model validation
$(document).ready(function(){
               
$("#change_password_form ").validate({
                rules: {
                 old_password: {
                 required: true,
                 remote: {
                 url: "<?php echo base_url('Admin/password_validate'); ?>",
                 type: "POST",
                 data: {
                 confirm_deact_password: function () {
                 return $('#old_password').val();
                        }
                      }
                     }
                  },
                  new_password: {
                     required: true,
                            },
                  confm_password: {
                        required: true,
                        equalTo : "#new_password" 
                            }
                        },
               messages: {
                old_password: {
                    required: "Current Password is Required",
                    remote: "Invalid Password"
                            },
                new_password: {
                    required: "New Password Required"
                    },
                confm_password: {
                    required: "Confirm Password required"
                            }
                        }
                        , errorElement: "div",
                        errorPlacement: function (error, element) {
                            error.insertAfter(element);
                        }
          });
        
         


            
            // $('.dataTables-example').DataTable({
            //     pageLength: 10,
            //     responsive: true,
            //     dom: '<"html5buttons"B>lTfgitp',
            //     buttons: [
            //         { extend: 'copy'},
            //         {extend: 'csv'},
            //         {extend: 'excel', title: 'ExampleFile'},
            //         {extend: 'pdf', title: 'Report'},

            //         {extend: 'print',
            //          customize: function (win){
            //                 $(win.document.body).addClass('white-bg');
            //                 $(win.document.body).css('font-size', '10px');

            //                 $(win.document.body).find('table')
            //                         .addClass('compact')
            //                         .css('font-size', 'inherit');
            //         }
            //         }
            //     ]

            // });

        });




    </script>
</body>
</html>
