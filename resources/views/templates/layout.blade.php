<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>@yield('title')</title>

        <link rel="icon" href="{{asset("resources/img/logo.png")}}" type="image/x-icon"/>
        <link href="{{asset("resources/css/bootstrap.min.css")}}" rel="stylesheet">
        <link href="{{asset("resources/css/login.css")}}" rel="stylesheet">
       <link href="{{asset("resources/font-awesome/css/font-awesome.css")}}" rel="stylesheet">
       <link href="{{asset("resources/css/plugins/toastr/jquery.toast.min.css")}}" rel="stylesheet">
        <link href="{{asset("resources/css/animate.css")}}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

        

    </head>

    <body class="gray-bg-login">

     
         @yield('content')
        <!-- Mainly scripts -->
        <script src="{{asset("resources/js/jquery-2.1.1.js")}}"></script>
        <script src="{{asset("resources/js/jquery.min.js")}}"></script>
        <script src="{{asset("resources/js/jquery.validate.min.js")}}"></script>
        
        <script src="{{asset("resources/js/bootstrap.min.js")}}"></script>
        <script src="{{asset("resources/js/plugins/slimscroll/jquery.slimscroll.min.js")}}""></script>
        <script src="{{asset("resources/js/plugins/toastr/jquery.toast.js")}}"></script>

               
       
    </body>

</html>
