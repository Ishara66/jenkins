<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Login@login');
Route::Post('/login', 'Login@authenticate_user');
Route::get('/dashboard', 'Dashboard@select_dashboard');


// Route::get('/', function () {
//     return view('login');
// });
